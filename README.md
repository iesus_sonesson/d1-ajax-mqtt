# AJAX Systems alarm controlled by an arduino over wifi with MQTT.

This simple arduino code can push the buttons off a space control for you.

What i used?

 - Dirty workbench
 - Ajax space control.
 - Soldering iron.
 - D1 Mini lite.
 - Hot glue gun.
 - a little bit less dirty table top
 - Czech beer

First off, i started with measuring a bit, on the space control.
I quickly found that the four outside  terminals are common ground, 
so i connected a wire to one of them.

While the four innermost needed a wire each.
I soldered those.

![picture](images/soldered-connections.jpg)

After which i put some hot glue  to hold the wires in place.

I went on to the positive wire, this was soldered onto the battery holder on the back 
side of the space control.

![picture](images/red-wire.jpg)

Now onto choosing a chip to control it with.

I had a few laying around, the form factor of the D1 mini was quite appealing so I 
choose one of those. (lite i might add, but does not really matter.)

![picture](images/all-set-for-coding.jpg)

I connected the white wires to the following pins on the board

```
PANIC -> D1
DISARM -> D2
ARM -> D6
NIGHT -> D7
```

Those were at some level chosen at random, but I intentionally  avoided including 
pin D3, RST as well as TX / RX, in order to be able to flash firmware over usb
 without disconnecting the wires.

Red wire was soldered to the 3.3v pin, and the black wire to GND.

After which i connected the chip to the computer and started putting a few things together mostly 
from old projects i suppose. (most of the code is really just getting it connected to mqtt and wifi,
subscribed to a topic etc.) 

Well after a few minutes i started testing the input/output high/low and as i suspected 
i need to send an output-LOW. From what I've heard it's not recommended to switch pinMode in loop,
 but I guess that's what I ended up doing. 

I connected it all up and tested it, worked like a charm.

Final result (without enclosure)

![picture](images/completed-project.jpg)
