#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define  PANIC   D1
#define  DISARM  D2
#define  ARM     D6
#define  NIGHT   D7

WiFiClient   espClient;
PubSubClient mqtt(espClient);
const char* ssid     = "YOUR_SSID";
const char* password = "YOUR_WIFI_PASSWORD";
const char* host     = "YOUR_MQTT_HOST";

void setup() {
  Serial.begin(115200);
  //built in led on because it looks good
  pinMode(LED_BUILTIN, OUTPUT);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print('.');
    delay(500);
  }
  mqtt.setServer(host, 1883);
  mqtt.setCallback(callback);
}

void reconnect() {
  while (!mqtt.connected()) {
    Serial.print("Attempting MQTT connection...");
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    if (mqtt.connect(clientId.c_str())) {
      mqtt.subscribe("ajax/alarm");
    } else {
      Serial.print("failed, rc=");
      Serial.print(mqtt.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
  Serial.println("MQTT Connected.");
}

void loop() {
  if (!mqtt.connected()) {
    reconnect();
  }
  mqtt.loop();
}


void callback(
  char* topic,
  byte* payload,
  unsigned int length
) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  String msg = "";
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    msg = msg+(char)payload[i];
  }

  if (strcmp(topic, "ajax/alarm") == 0 && msg == "panic") {
    pressButton(PANIC);
  } else if (strcmp(topic, "ajax/alarm") == 0 && msg == "disarm") {
    pressButton(DISARM);
  } else if (strcmp(topic, "ajax/alarm") == 0 && msg == "arm") {
    pressButton(ARM);
  } else if (strcmp(topic, "ajax/alarm") == 0 && msg == "night") {
    pressButton(NIGHT);
  }
}

void pressButton(int buttonId) {
  pinMode(buttonId, OUTPUT);
  digitalWrite(buttonId, LOW);
  delay(300);
  pinMode(buttonId, INPUT);
  Serial.println("SENT SIGNAL");
}
